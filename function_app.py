import azure.functions as func
import logging

from math import sin
import json


app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)


PARTITIONS = [10, 100, 1_000, 10_000, 100_000, 1_000_000]
USE_CACHE = False


cache: dict[tuple[float, float], dict[int, float]] = {}


def compute_integral(lower: float, upper: float, n: int = 1000) -> float:
    def f(x): return abs(sin(x))

    integral = 0
    delta = (upper - lower) / n
    a = lower
    b = lower + delta
    for i in range(n):
        integral += 0.5 * (b - a) * (f(a) + f(b))
        a = b
        b = a + delta

    return integral


def get_error(message: str, code: int) -> func.HttpResponse:
    return func.HttpResponse(json.dumps({'message': message, 'code': code}), status_code=code)


def get_result(lower, upper) -> dict[int, float]:
    return {i: compute_integral(lower, upper, n=i) for i in PARTITIONS}


@app.route(route="numerical_integration")
def numerical_integration(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    try:
        lower = float(req.params.get("lower"))
        upper = float(req.params.get("upper"))
    except Exception:
        return get_error("'lower' and 'upper' parameters are required and must be float", 400)

    if lower is None or upper is None:
        return get_error("'lower' and 'upper' parameters are required and must be float", 400)

    if lower > upper:
        return get_error("'lower' parameter must be lower than 'upper' parameter", 400)

    if not USE_CACHE:
        return func.HttpResponse(json.dumps(get_result(lower, upper)), status_code=200)

    if (lower, upper) not in cache:
        cache[(lower, upper)] = get_result(lower, upper)

    return func.HttpResponse(json.dumps(cache[(lower, upper)]), status_code=200)
